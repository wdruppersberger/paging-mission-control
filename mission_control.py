from datetime import datetime, timedelta
import json
import sys
import csv


def get_data(filename):
	"""
	@summary: Retrieves the data from the input file
	@param filename: The input file with the telemetry data
	@returns: The data in a readable format ready for iteration
	"""
	file = open(filename, "r")
	tele_data = csv.reader(file, delimiter = '|')
	return tele_data

def create_alert(satID, component, time):
	"""
	@summary: Creates an alert message when there is a violation and outputs it to the console
	@param satID: The id number of the satellite with the violation
	@param component: The component of the satellite (either temperature or battery)
	@param time: The time in which the first violation occured
	@returns: Prints the alert message to the console
	"""
	if component == 'TSTAT':
		severity = "RED HIGH"
	else:
		severity = "RED LOW"
	#create dictionary for alert message
	alert_dict = {
		"_satelliteId": int(satID),
        "severity": severity,
        "component": component,
        "timestamp": datetime.strftime(time, '%Y-%m-%dT%H:%M:%S:%f')[:-3]+'Z'
	}
	print(json.dumps(alert_dict, indent=4, sort_keys=True))
	
def alert_check(filename):
	"""
	@summary: Iterates through the telemetry data to identify if any violations are occuring that require an alert message
	@param filename: The input file with the telemetry data
	"""

	#storing the data in a readable format
	tele_data = get_data(filename)
	#creating variables to track the number of violations within five minutes
	count = 0
	timer = 0
	alert = None
	countB = 0
	timerB = 0
	alertB = None
	#iterating through each line of the telemetry data looking for violations
	for sat_data in tele_data:

		#storing the necessary data to find a violation from each line 
		time = datetime.strptime(sat_data[0], '%Y%m%d %H:%M:%S.%f')
		satID = sat_data[1]
		component = sat_data[7]
		raw = float(sat_data[6])
		redLow = int(sat_data[5])
		redHigh = int(sat_data[2])

		#checking the temperature component of the satellite to see if it is above the limit
		if component == 'TSTAT':
			if raw > redHigh:
				#first violation, start the counter and timer and store satellite data
				if count == 0:
					count += 1
					alert = sat_data
					timer = time + timedelta(minutes = 5)
				#second or thid violation within the time frame of five minutes; adds to counter
				elif time <= timer:
					count += 1
				#if second or third violation occurs outside of five minutes of first; reset the timer and counter
				#and store new satellite data
				elif time > timer:
					count = 1
					timer = time + timedelta(minutes = 5)
					alert = sat_data
				#generate alert for temperature component of satellite if three violations occur within five minutes
				if count == 3:
					count = 0
					create_alert(alert[1], alert[7], datetime.strptime(alert[0], '%Y%m%d %H:%M:%S.%f'))

		#checking the battery component of the satellite to see if it is below the limit
		elif component == 'BATT':
			if raw < redLow:
				#first violation, start the counter and timer and store satellite data
				if countB == 0:
					countB += 1
					alertB = sat_data
					timerB = time + timedelta(minutes = 5)
				#second or thid violation within the time frame of five minutes; adds to counter
				elif time <= timerB:
					countB += 1
				#if second or third violation occurs outside of five minutes of first; reset the timer and counter
				#and store new satellite data
				elif time > timerB:
					countB = 1
					timerB = time + timedelta(minutes = 5)
					alertB = sat_data
				#generate alert for battery component of satellite if three violations occur within five minutes
				if countB == 3:
					countB = 0
					create_alert(alertB[1], alertB[7], datetime.strptime(alertB[0], '%Y%m%d %H:%M:%S.%f'))
					

if __name__ == "__main__":
        alert_check(sys.argv[1])